# About #
This script checks the age of the last file in a directory. File must not be older than the age defined by the user. Otherwise, it will exit with error (or alarm if used with Nagios).

# Prerequisite #
find (GNU findutils) 4.4.0

# Usage #
```
#!bash
Usage: ./check_age_last_file.sh -d directory -f filename -a age [-h help]
```
