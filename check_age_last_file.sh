#!/bin/bash
#------------------------------------------------
# Script: check_age_last_file.sh
# Author: John Paul P. Doria
#------------------------------------------------
# Feature:
# - Check the age of the file in the directory.
#------------------------------------------------
# Changelog:
# - 1.0 (2014-05-20)
#   Initial script for checking the age of the
#   last file in the directory.
#------------------------------------------------
# Requirement:
# - find (GNU findutils) 4.4.0
#------------------------------------------------
# Instruction :
# ./check_age_last_file.sh -d directory -f filename -a age [-h help]
#------------------------------------------------

usage() {
        echo "Usage: $0 -d directory -f filename -a age [-h help]"
}

while getopts ":d:f:a:h" opt; do
        case $opt in
                d)      directory=$OPTARG
                        ;;
                f)      filename=$OPTARG
                        ;;
                a)      age=$OPTARG
                        ;;
                h)      usage
                        exit 1
                        ;;
                \?)     echo "Invalid option: -$OPTARG"
                        usage
                        exit 1
                        ;;
                :)      echo "Error: option -$OPTARG requires an argument."
                        usage
                        exit 1
                        ;;
        esac

done

# Exit if all variables are null.
if [[ -z $directory || -z $filename || -z $age ]]; then
        usage
        exit 1
fi

file=`ls -1 $directory/$filename 2>/dev/null | tail -1`
file_count=`ls -1 $directory/$filename 2>/dev/null | tail -1 | wc -l`

# Check diretory if existing.
if [ ! -d $directory ]; then
        echo "Error: $directory not found."
        exit 1
fi

# Check age. File must not be older than age defined by user.
if [ $file_count -eq 0 ]; then
        echo "Error: cannot access $filename: No such file or directory"
        exit 1
else
        if [[ `find $file -mmin +$age` ]]; then
                echo "Error: `basename $file` is older than $age minute(s)."
                exit 2
        else
                echo "OK: `basename $file` is the newest file."
                exit 0
        fi
fi